package rest;

import services.PoliceService;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

@Path("/police")
public class PoliceRest {

    @Inject
    private PoliceService policeService;

    @POST
    @Path("/add")
    @Consumes("text/plain")
    public void addTrackID(String serialID){
        this.policeService.addNewTrackID(serialID);
    }

    @GET
    @Path("/track")
    @Produces("application/json")
    public List<String> getTrackIds(){
        return policeService.getTrackIds();
    }

    @GET
    @Path("/test")
    public void test(){
        this.policeService.testMessage();
    }
}
