package jms;

import com.owlike.genson.Genson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import dto.TransLocationDto;
import dto.TransLocationMapper;
import org.json.JSONObject;
import services.TransLocationService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@Startup
@Singleton
public class MessageReceiver {
    private Gateway receiveFromSimulation;
    private static final String QUEUE_NAME = "SimulationToItaly";
    @Inject
    private TransLocationService transLocationService;

    public MessageReceiver() {

    }

    @PostConstruct
    public void init(){
        try {
            receiveFromSimulation = new Gateway();
            receiveFromSimulation.channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            setupReceivehandler();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private String convertBytesToPayLoad(byte[] input) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(input);
        ObjectInputStream reader = new ObjectInputStream(bais);
        return (String) reader.readObject();
    }

    private void setupReceivehandler(){
        Consumer consumer = new DefaultConsumer(receiveFromSimulation.channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                System.out.println("Got something..");
                String payLoad = null;
                try {
                    payLoad = convertBytesToPayLoad(body);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                JSONObject jsonObj = new JSONObject(payLoad);
                TransLocationDto dto = new TransLocationDto(
                        jsonObj.get("serialNumber").toString(),
                        jsonObj.get("lat").toString(),
                        jsonObj.get("lon").toString(),
                        jsonObj.get("timestamp").toString(),
                        jsonObj.get("countryCode").toString());
                transLocationService.save(TransLocationMapper.mapTranslocationDtoToObject(dto));
                System.out.println("serial number: " + dto.getSerialNumber() + ", lat: " + dto.getLat() + ", lon: " + dto.getLon());
            }
        };
        try {
            receiveFromSimulation.channel.basicConsume(QUEUE_NAME, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
