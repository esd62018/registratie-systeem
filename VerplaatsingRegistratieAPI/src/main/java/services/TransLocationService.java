package services;

import dao.TransLocationDao;
import models.Journey;
import models.TransLocation;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class TransLocationService {

    @Inject
    private TransLocationDao transLocationDao;

    public TransLocationService() {
    }

    public void save(TransLocation transLocation){
        transLocationDao.save(transLocation);
    }


    public TransLocation getById(int id) {
        return this.transLocationDao.getById(id);
    }

}
