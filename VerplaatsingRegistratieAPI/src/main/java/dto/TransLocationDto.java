package dto;

import models.TransLocation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class TransLocationDto{
    private String serialNumber;
    private String lat;
    private String lon;
    private String timestamp;
    private String countryCode; //Country the car is driving in

    public TransLocationDto(TransLocation transLocation) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        String datetime = df.format(transLocation.getDateTime());
        this.serialNumber = transLocation.getSerialNumber();
        this.lat = transLocation.getLat().toString();
        this.lon = transLocation.getLon().toString();
        this.timestamp = datetime;
        this.countryCode = transLocation.getCountryCode();
    }

    public TransLocationDto(String serialNumber, String lat, String lon, String timestamp, String countryCode) {
        this.serialNumber = serialNumber;
        this.lat = lat;
        this.lon = lon;
        this.timestamp = timestamp;
        this.countryCode = countryCode;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
