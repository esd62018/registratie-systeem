package models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(
                name="TransLocation.findByID",
                query="SELECT t FROM TransLocation t WHERE t.id = :id"
        )
})
public class TransLocation{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    private Double lat;
    private Double lon;
    private Date dateTime;
    private String serialNumber;
    private String countryCode;
    @ManyToOne
    @JoinColumn(name = "JOURNEY_ID")
    private Journey journey;

    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public TransLocation() {
    }

    public TransLocation(Double lat, Double lon, Date dateTime, String serialNumber, String countryCode) {
        this.lat = lat;
        this.lon = lon;
        this.dateTime = dateTime;
        this.serialNumber = serialNumber;

        this.countryCode = countryCode;
    }

    public Double getLat() {
        return this.lat;
    }

    public Double getLon() {
        return this.lon;
    }

    public Date getDateTime() {
        return this.dateTime;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
