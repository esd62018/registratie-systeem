package dto;

import models.Journey;
import models.TransLocation;
import services.JourneyService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class JourneyMapper {
    private JourneyService journeyService;

    public JourneyMapper(JourneyService journeyService){
        this.journeyService = journeyService;
    }

    public JourneyDto mapJourneyDto(Journey journey){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        String endTime = df.format(journey.getEndTime());
        JourneyDto journeyDto = new JourneyDto(journey.getId().toString(), journey.getSerialNumber().toString(), endTime);
        for(TransLocation t : journey.getTransLocations()){
            journeyDto.addTranslocation(t.getId().toString());
        }
        return journeyDto;
    }
}
