package models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(
                name="Journey.findByID",
                query="SELECT j FROM Journey j WHERE j.id = :id"
        ),
        @NamedQuery(
                name="Journey.findBySerialNumberAndDates",
                query="SELECT j FROM Journey j WHERE j.serialNumber = :serialNumber AND j.endTime > :startdate AND j.endTime < :enddate"
        ),
        @NamedQuery(
                name="Journey.getAll",
                query="SELECT j FROM Journey j"
        )

})
public class Journey{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String serialNumber;
    private Date endTime;
    @OneToMany(mappedBy = "journey")
    private List<TransLocation> transLocationList;

    public Journey() {
        this.transLocationList = new ArrayList<TransLocation>();
    }

    public Journey(TransLocation newLocation) {
        this.transLocationList = new ArrayList<TransLocation>();
        this.addTranslocation(newLocation);
        this.serialNumber =  newLocation.getSerialNumber();
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<TransLocation> getTransLocations() {
        return this.transLocationList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<TransLocation> getTransLocationList() {
        return transLocationList;
    }

    public void setTransLocationList(List<TransLocation> transLocationList) {
        this.transLocationList = transLocationList;
    }

    public void addTranslocation(TransLocation transLocation){
        transLocation.setJourney(this);
        this.transLocationList.add(transLocation);

    }
}
