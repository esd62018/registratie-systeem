package dao;

import models.Journey;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class JourneyDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Journey> getAllJourneys() {
        List<Journey> journeys =  entityManager.createNamedQuery("Journey.getAll")
                .getResultList();
        return journeys;
    }

    public void save(Journey newJourney) {
        Date lastDate = newJourney.getTransLocations().get(newJourney.getTransLocations().size()-1).getDateTime();
        newJourney.setEndTime(lastDate);
        entityManager.persist(newJourney);
    }

    public Journey getJourneyById(int i) {
        Journey journey = (Journey) entityManager.createNamedQuery("Journey.findByID")
                .setParameter("id", i)
                .getSingleResult();
        return journey;
    }

    public List<Journey> getJourneysBySerialnumberAndDates(String serialNumber, String startdate, String enddate) {
        List<Journey> journeys = entityManager.createNamedQuery("Journey.findBySerialNumberAndDates")
                .setParameter("serialNumber", serialNumber)
                .setParameter("startdate", startdate)
                .setParameter("enddate", enddate)
                .getResultList();
        return journeys;
    }
}
