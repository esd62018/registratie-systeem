package services;

import dao.JourneyDao;
import models.Journey;
import models.TransLocation;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class JourneyService {

    @Inject
    private TransLocationService transLocationService;
    @Inject
    private JourneyDao journeyDao;

    public List<Journey> getAllJourneys() {
        return journeyDao.getAllJourneys();
    }

    public void createJourney(TransLocation transLocation){
        Journey newJourney = new Journey(transLocation);
        this.transLocationService.save(transLocation);
        this.journeyDao.save(newJourney);
    }

    public Journey getJourneyById(int i) {
        return this.journeyDao.getJourneyById(i);
    }

    public List<Journey> getJourneysBySerialnumberAndDates(String serialNumber, String startdate, String enddate) {
        return this.journeyDao.getJourneysBySerialnumberAndDates(serialNumber, startdate, enddate);
    }
}
