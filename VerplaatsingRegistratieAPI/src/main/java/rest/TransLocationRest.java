package rest;

import dto.TransLocationDto;
import dto.TransLocationMapper;
import models.TransLocation;
import services.TransLocationService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/translocation")
public class TransLocationRest {

    @Inject
    private TransLocationService transLocationService;

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public TransLocationDto getAllJourneys(@PathParam("id") int id) {
        TransLocation transLocation = this.transLocationService.getById(id);
        return TransLocationMapper.mapTransLocationDto(transLocation);
    }
}

