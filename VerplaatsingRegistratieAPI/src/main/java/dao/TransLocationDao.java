package dao;


import models.TransLocation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TransLocationDao {
    @PersistenceContext
    private EntityManager entityManager;

    public TransLocationDao() {
    }

    public void save(TransLocation transLocation){
        entityManager.persist(transLocation);
    }

    public TransLocation getById(int id) {
        TransLocation transLocation = (TransLocation) entityManager.createNamedQuery("TransLocation.findByID")
                .setParameter("id", id)
                .getSingleResult();
        return transLocation;
    }
}
