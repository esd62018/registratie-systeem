package dto;

import java.util.ArrayList;
import java.util.List;

public class JourneyDto {
    private String id;
    private String serialNumber;
    private String endDate;
    private List<String> transLocationUris;

    public JourneyDto() {
    }

    public JourneyDto(String id, String serialNumber, String endDate) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.endDate = endDate;
        this.transLocationUris = new ArrayList<String>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<String> getTransLocationUris() {
        return transLocationUris;
    }

    public void setTransLocationUris(List<String> transLocationUris) {
        this.transLocationUris = transLocationUris;
    }

    public void addTranslocation(String translocationID){
        String uri = "<uri>/"+translocationID;
        this.transLocationUris.add(uri);
    }
}
