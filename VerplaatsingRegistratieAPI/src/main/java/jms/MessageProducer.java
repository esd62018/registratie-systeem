package jms;

import dto.TransLocationDto;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

@Stateless
public class MessageProducer {
    private static Gateway dataToPolice;
    private static final String channelName  = "dataToPolice";

    public MessageProducer(){

    }

    public static void sendLocationList(List<TransLocationDto> transLocationDtos){
        for (int i =0; i<transLocationDtos.size(); i++){
            MessageProducer.sendNewLocation(transLocationDtos.get(i));
        }
    }

    public static void sendNewLocation(TransLocationDto transLocationDto){
        JSONObject jsonObject =  new JSONObject(transLocationDto);
        try {
            dataToPolice.channel.basicPublish("",channelName, null, convertPayLoadToBytes(jsonObject.toString()));
            System.out.println("Message send: " + transLocationDto.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static byte[] convertPayLoadToBytes(String payload) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream writter = new ObjectOutputStream(baos);
        writter.writeObject(payload);
        return baos.toByteArray();
    }

    private static void startSimulationToPolice(){
        try {
            dataToPolice = new Gateway();
            dataToPolice.channel.queueDeclare(channelName, false, false, false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void init() {
        startSimulationToPolice();
    }
}
