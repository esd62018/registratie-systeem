package rest;

import dto.JourneyDto;
import dto.JourneyMapper;
import models.Journey;
import models.TransLocation;
import services.JourneyService;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/journeys")
public class JourneyRest {

    @Inject
    private JourneyService journeyService;

    @GET
    @Produces("application/json")
    public List<JourneyDto> getAllJourneys() {
        List<Journey> journeys =  journeyService.getAllJourneys();
        List<JourneyDto> journeyDtos = new ArrayList<>();
        for(Journey j : journeys){
            journeyDtos.add(new JourneyMapper(journeyService).mapJourneyDto(j));
        }
        return journeyDtos;
    }

    @GET
    @Produces("application/json")
    @Path("/{serialNumber}?startdate={startdate}&enddate={enddate}")
    public List<JourneyDto> getAllJourneys(@PathParam("serialNumber") String serialNumber,@PathParam("startdate") String startdate, @PathParam("enddate") String enddate) {
        List<Journey> journeys =  journeyService.getJourneysBySerialnumberAndDates(serialNumber, startdate, enddate);
        List<JourneyDto> journeyDtos = new ArrayList<>();
        for(Journey j : journeys){
            journeyDtos.add(new JourneyMapper(journeyService).mapJourneyDto(j));
        }
        return journeyDtos;
    }

    @Path("/new")
    @POST
    @Consumes("application/json")
    public void newJourney(TransLocation transLocation){
       this.journeyService.createJourney(transLocation);
    }
}
