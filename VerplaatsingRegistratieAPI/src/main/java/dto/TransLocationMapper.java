package dto;

import models.TransLocation;
import services.TransLocationService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;

public class TransLocationMapper {

    public TransLocationMapper(){
    }

    public static TransLocationDto mapTransLocationDto(TransLocation transLocation) {
        return new TransLocationDto(transLocation);
    }

    public static TransLocation mapTranslocationDtoToObject(TransLocationDto transLocationDto){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(transLocationDto.getTimestamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  new TransLocation(Double.valueOf(transLocationDto.getLat()), Double.valueOf(transLocationDto.getLon()), date, transLocationDto.getSerialNumber(), transLocationDto.getCountryCode());
    }
}
