package services;

import dao.PoliceDao;
import dto.TransLocationDto;
//import jms.MessageProducer;
import models.Journey;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class PoliceService {
    @Inject
    private PoliceDao policeDao;
    @Inject
    private JourneyService journeyService;

    public PoliceService(){

    }

    public void addNewTrackID(String serialID){
        this.policeDao.addNewTrackID(serialID);
    }

    public boolean checkIfNeedsToBeSendToPolice(String serialID){
        return this.policeDao.checkIfNeedsToBeSendToPolice(serialID);
    }


    public List<String> getTrackIds() {
        return policeDao.getTrackIds();
    }

    public void testMessage() {
        Journey journey = journeyService.getJourneyById(1);
        List<TransLocationDto> transLocationDtos = new ArrayList<TransLocationDto>();
        for(int i =0; i < journey.getTransLocations().size(); i++){
            transLocationDtos.add(new TransLocationDto(journey.getTransLocations().get(i)));
        }
        //MessageProducer.sendLocationList(transLocationDtos);
    }
}
