package dao;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class PoliceDao {
    private List<String> trackSerialIds;

    public PoliceDao(){
        this.trackSerialIds = new ArrayList<String>();
        //this.addNewTrackID("1");
        //this.addNewTrackID("2");
    }

    public void addNewTrackID(String serialID){
        this.trackSerialIds.add(serialID);
    }

    public boolean checkIfNeedsToBeSendToPolice(String serialID){
        return trackSerialIds.contains(serialID);
    }

    private void setupTrackids(){
        this.trackSerialIds = new ArrayList<String>();
    }

    public List<String> getTrackIds() {
        return this.trackSerialIds;
    }
}
